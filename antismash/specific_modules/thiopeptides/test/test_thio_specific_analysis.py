try:
    import unittest2 as unittest
except ImportError:
    import unittest
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from Bio.SeqFeature import SeqFeature, FeatureLocation
from minimock import mock, restore, TraceTracker
from antismash import utils
from antismash.specific_modules.thiopeptides.specific_analysis import (
    Thiopeptide,
    predict_cleavage_site,
    result_vec_to_features,
)

class TestThiopeptide(unittest.TestCase):
    def test_init(self):
        "Test Thiopeptide instantiation"
        thio = Thiopeptide(20, 31, 32)
        self.assertTrue(isinstance(thio, Thiopeptide))
        self.assertEqual(20, thio.start)
        self.assertEqual(31, thio.end)
        self.assertEqual(32, thio.score)
        self.assertEqual('', thio.thio_type)
        self.assertEqual('', thio.core)
        with self.assertRaises(ValueError):
            thio.molecular_weight

    def test_repr(self):
        "Test Thiopeptide representation"
        thio = Thiopeptide(20, 31, 32)
        thio.core = "MAGIC"
        expected = "Thiopeptide(20..31, 32, 'MAGIC', '', -1(-1), , False, , )"
        self.assertEqual(expected, repr(thio))

            
    def test_core(self):
        "Test Thiopeptide.core"
        thio = Thiopeptide(20, 31, 32)
        self.assertEqual('', thio.core)
        self.assertFalse(hasattr(thio, 'core_analysis'))
        thio.core = "MAGICHAT"
        self.assertEqual('MAGICHAT', thio.core)
        self.assertTrue(hasattr(thio, 'core_analysis'))

    def test_core_ignore_invalid(self):
        "Test Thiopeptide.core ignores invalid amino acids"
        thio = Thiopeptide(20, 31, 32)
        self.assertEqual('', thio.core)
        self.assertFalse(hasattr(thio, 'core_analysis'))
        thio.core = "MAGICXHAT"
        self.assertEqual('MAGICHAT', thio.core)
        self.assertTrue(hasattr(thio, 'core_analysis'))

    def test_macrocycle_size(self):
        "Test Thiopeptide._predict_macrocycle"
        thio = Thiopeptide(20, 31, 32)
        thio.core = "SAAAAAAAASC"
        self.assertEqual('26-member', thio.macrocycle)
        thio.core = "SAAAAAAAAASSSSSS"
        self.assertEqual('35-member', thio.macrocycle)
        thio.core = "SDDDDDDDDDSC"
        self.assertEqual('29-member', thio.macrocycle)
        thio.core = "TAAASDDDDDDDDSCDD"
        self.assertEqual('26-member', thio.macrocycle)


    def test_weights(self):
        "Test Thiopeptide.alt_mature_weights"
        # includes all weights (even after maturation)
        thio = Thiopeptide(23, 42, 17)
        thio.core = "MAGICCHATS"
        thio.amidation = True
        thio.thio_type = "Type-I"

        mat_weights = thio.mature_alt_weights
        mw = mat_weights[0]
        mono = mat_weights[1]
        alt = mat_weights[2:]
        self.assertAlmostEqual(mw, 1051.1623)
        self.assertAlmostEqual(mono, 1050.367788)
        self.assertAlmostEqual(alt, [1069.1823, 1087.2023])

        thio.core = "MAGICCHATS"
        thio.amidation = False
        thio.thio_type = "Type-I"
        #reset mature alt weights to calculate them again
        thio._mature_alt_weights = []

        mat_weights = thio.mature_alt_weights
        mw = mat_weights[0]
        mono = mat_weights[1]
        alt = mat_weights[2:]
        self.assertAlmostEqual(mw, 1121.1623)
        self.assertAlmostEqual(mono, 1120.367788)
        self.assertAlmostEqual(alt, [1139.1823, 1157.2023])

        thio.core = "MAGICCHATS"
        thio.amidation = True 
        thio.thio_type = "Type-II"
        #reset mature alt weights to calculate them again
        thio._mature_alt_weights = []

        mat_weights = thio.mature_alt_weights
        mw = mat_weights[0]
        mono = mat_weights[1]
        alt = mat_weights[2:]
        self.assertAlmostEqual(mw, 1097.1623)
        self.assertAlmostEqual(mono, 1096.367788)
        self.assertAlmostEqual(alt, [1115.1823, 1133.2023])
        
class TestSpecificAnalysis(unittest.TestCase):
    class FakeHit(object):
        class FakeHsp(object):
            def __init__(self, start, end, score):
                self.query_start = start
                self.query_end = end
                self.bitscore = score

        def __init__(self, start, end, score, desc):
            self.hsps = [ self.FakeHsp(start, end, score) ]
            self.description = desc

        def __iter__(self):
            return iter(self.hsps)

    def setUp(self):
        self.tt = TraceTracker()
        self.hmmpfam_return_vals = []
        mock('utils.run_hmmpfam2', tracker=self.tt, returns=self.hmmpfam_return_vals)

    def tearDown(self):
        restore()

    def test_predict_cleavage_site(self):
        "Test thiopeptides.predict_cleavage_site()"
        resvec = predict_cleavage_site('foo', 'bar')
        self.assertEqual(None, resvec)
        fake_hit = self.FakeHit(24, 42, 17,'fake')
        self.hmmpfam_return_vals.append([fake_hit])

        res = predict_cleavage_site('foo', 'bar')

        self.assertEqual(24, res.start)
        self.assertEqual(28, res.end)
        self.assertEqual(17, res.score)
        self.assertEqual('', res.thio_type)


    def test_result_vec_to_features(self):
        "Test thiopeptides.result_vec_to_features()"
        loc = FeatureLocation(0, 165, strand=1)
        orig_feature = SeqFeature(loc, 'CDS')
        orig_feature.qualifiers['locus_tag'] = ['FAKE0001']
        vec = Thiopeptide(17, 23, 42)
        seq = 'SCTSSCTSS'
        vec.thio_type = 'Type-III'
        vec.core = seq
        vec.leader = "HEADHEADHEAD"
        new_features = result_vec_to_features(orig_feature, vec)
        self.assertEqual(2, len(new_features))
        leader, core = new_features

        self.assertEqual(loc.start, leader.location.start)
        self.assertEqual(loc.start + (23 * 3), leader.location.end)
        self.assertEqual(loc.strand, leader.location.strand)
        self.assertEqual('CDS_motif', leader.type)
        self.assertEqual(orig_feature.qualifiers['locus_tag'],
                         leader.qualifiers['locus_tag'])
        self.assertEqual(['leader peptide', 'thiopeptide',
                          'predicted leader seq: HEADHEADHEAD'], leader.qualifiers['note'])

        self.assertEqual(leader.location.end, core.location.start)
        self.assertEqual(loc.end, core.location.end)
        self.assertEqual(loc.strand, core.location.strand)
        self.assertEqual('CDS_motif', core.type)
        expected = ['core peptide', 'thiopeptide', 'monoisotopic mass: 861.3',
                    'molecular weight: 861.9',
                    'alternative weights: 879.9; 897.9; 916.0; 934.0; 952.0; 970.0; 988.0',
                    'predicted core seq: SCTSSCTSS',
                    'predicted type: Type-III',
                    'score: 42.00',
                    'predicted macrocycle: ',
                    'putative cleaved off residues: ',
                    'predicted core features: Central ring- pyridine trisubstituted']
        self.assertEqual(expected, core.qualifiers['note'])
        self.assertEqual(orig_feature.qualifiers['locus_tag'],
                         core.qualifiers['locus_tag'])


 
